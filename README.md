Battle Tanks game in section 4 of Ben Tristem's Unreal Engine 4 course on Udemy.
 
 ## Lecture List
 * BT01 Intro, Notes & Assets
 * BT02 Game Design Document (GDD) 
 * BT03 Setting Up a GitHub "Repo"
 * BT04 Creating & Deleting Landscapes
 * BT05 A Landscaping Process
 * BT06 Adding Boundary and Texture to Landscape
 * BT07 Adding Tags to Commits
 * BT08 Using Landscape Layers
 * BT09 Adding To Landscape
 * BT10 Actors from Multiple Meshes
 * BT11 Configuring Tank & Adding 3rd Person Camera Rotation
 * BT12 Fixing 3rd Person Camera and Adding UI Dot
 * BT13 Main Menu Screens
 * BT14 UI Scale Box, Buttons, Mouse, & Trial Packaging
 * BT15 Delating Components & Using Virtual and Override
 * BT16 Creating an AI Controller Class
 * BT17 Get the Player Controller with C++
 * BT18 Adding Tick() and Creating an Out Parameter Method
 * BT19 Finding Screen Pixel Coordinates
 * BT20 Using DeprojectScreentoWorld
 * BT20 Using LineTraceSingleByChannel()
 * BT22 Unify Player & AI Aiming
 * BT23 Creat Default Sub Objects in C++
 * BT24 Blueprint Callable()
 * BT25 Using SuggestProjectileVelocity()
 * BT26 Using Forward Declarations
 * BT27 BlueprintSpawnableComponent()
 * BT28 Review and Refining
 * BT29 Fixing Bugs & Adding Forward Declarations
 * BT30 Using Clamp() & Elevating Barrel
 * BT31 Turret Rotation
 * BT32 Setting Up Projectiles and Firing
 * BT33 Spawning Projectile
 * BT34 Projectile Movement Components
 * BT35 Making AI Tanks Fire
 * BT36 Adding EditDefaultsOnly
 * BT37 Adding Quit Button & Setting Up Track Throttles
 * BT38 Using ApplyForceAtLocation()
 * BT39 Physics Materials & Friction
 * BT40 Setting Up Fly-by-Wire Control System
 * BT41 Using BlueprintReadOnly
 * BT42 Finishing Fly-by-Wire Movement
 * BT43 Adding NavMeshBoundsVolume for AI Pathfinding
 * BT44 Dissecting RequestDirectMove()
 * BT45 Adding AI Tank Movement and Turning
 * BT46 Finalizing TankMovementComponent Class
 * BT47 Creating Blueprint Variable
 * BT48 Using Enums to Change Color of Crosshair
 * BT49 Refactoring Code and Changing Component Structure
 * BT50 Adding Ensure for Pointer Protection
 * BT51 Making Aiming Work Again
 * BT52 Refactoring Aiming Without Tank
 * BT53 Finishing Refactoring and Getting Firing Working Again
 * BT54 Changing Crosshair Color Based on Aiming Status
 * BT55 Fixing Sideways Movement
 * BT56 Registering OnComponentHit Event
 * BT57 Removing Forces in Air
 * BT58 Improving Tank Aiming
 * BT59 Tweaking Tank AI
 * BT60 Making an Ammo Display
 * BT61 Making an AutoMortar
 * BT62 Deleting Starter Content
 * BT63 Preparing for Particles
 * BT64 Introducing Particle System
 * BT65 Adding Impact Blast
 * BT66 Radial Forces & Caching
 * BT67 Deleting Projectiles After Use
 * BT68 Adding Tank Damage
 * BT69 Adding Health Bar
 * BT70 Broadcasting Death
 * BT71 Finishing Off
 * BT72 Camera Switching
 * BT73 Setting Up Win Condition and Looping Game
 * BT74 Continuing Win Condition Handling