// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"
#include "TankAIController.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API ATankAIController : public AAIController
{
	GENERATED_BODY()
	
protected:
	// How close can the AI tank get
	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float AcceptanceRadius = 8000;

	UFUNCTION(BlueprintCallable, Category = Result)
	bool GetIsGameOver() const;

	UFUNCTION(BlueprintCallable, Category = Result)
	void SetTanksDied();

private:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	virtual void SetPawn(APawn* InPawn) override;

	UFUNCTION()
	void OnTankDeath();

	static bool bIsGameOver;

	static int32 TanksDead;
};
