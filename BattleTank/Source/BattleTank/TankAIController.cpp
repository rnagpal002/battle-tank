// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "TankAimingComponent.h"
#include "Classes/Engine/World.h"
#include "Tank.h" 
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/GameFramework/Pawn.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"

// Depends on movement component via pathfinding system

int32 ATankAIController::TanksDead = 0;
bool ATankAIController::bIsGameOver = false;

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();

	TanksDead = 0;
}

void ATankAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!ensure(PossessedTank)) { return; }

		// Subscribe our local method ot the tank's death event
		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnTankDeath);
	}
}

void ATankAIController::OnTankDeath()
{
	if (!GetPawn()) { return; }
	SetTanksDied();
	GetPawn()->DetachFromControllerPendingDestroy();
}

// Called every frame
void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn();
	auto ControlledTank = GetPawn();

	if (!ensure(PlayerTank && ControlledTank)) { return; }

	// Move towards the player
	MoveToActor(PlayerTank, AcceptanceRadius); // TODO check radius is in cm

	auto AimingComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(AimingComponent)) { return; }

	// Aim towards the player
	AimingComponent->AimAt(PlayerTank->GetActorLocation());

	if (AimingComponent->GetFiringState() == EFiringStatus::Locked)
	{
		//AimingComponent->Fire(); // TODO limit firing rate
	}
}

void ATankAIController::SetTanksDied() {
	TanksDead++; 
	if (TanksDead >= 1)
	{
		TanksDead = 0;
		bIsGameOver = true;
		UE_LOG(LogTemp, Warning, TEXT("Game Over"));
	}
}

bool ATankAIController::GetIsGameOver() const { return bIsGameOver; }
