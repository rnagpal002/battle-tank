// Copyright EmbraceIT Ltd.

#include "Tank.h"
#include "TankBarrel.h"
#include "Projectile.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CurrentHealth = StartingHealth;
}

void ATank::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = StartingHealth;
}

float ATank::TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, class AController *EventInstigator, AActor *DamageCauser)
{
	int32 DamageToApply = FPlatformMath::RoundToInt(DamageAmount);
	DamageToApply = FMath::Clamp(DamageToApply, 0, CurrentHealth);

	CurrentHealth -= DamageToApply;
	if (CurrentHealth <= 0)
	{
		OnDeath.Broadcast();
	}

	return DamageToApply;
}

float ATank::GetHealthPercent() const { return (float)CurrentHealth / (float)StartingHealth; }
